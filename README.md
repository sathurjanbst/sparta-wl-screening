# Sparta-watchlist-screening on GCP

Sparta-watchlist-screening Google Cloud Deployment.

## Installation

Clone the gcloud_deployment repo from [here](https://bitbucket.org/bst_element/gcloud-deployments/src/master/) to configure deployment files.

## Creating Docker Image

1. Get the latest the local version into CI server, and run

```bash
$docker build --rm --no-cache -t sparta-wl-screening:1.0.5 .
```

2. Tag the image on the CI server, and run

```bash
$docker tag "sparta-wl-screening:1.0.5" "gcr.io/blackswan-161813/sparta-wl-screening:1.0.5"
```

3. Push the docker image 

```bash
$docker push  "gcr.io/blackswan-161813/sparta-wl-screening:1.0.5"
```

## Configuring .yml file

4. Then get a copy of sample-deployment.yml and sample-services.yml and edit the files accordingly.
5. In sample-deployment.yml -
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sparta-wl-screening-deployment
  labels:
    app: sparta-wl-screening
spec:
  replicas: 1
  selector:
    matchLabels: 
      app: sparta-wl-screening
  template:
    metadata:
      labels:
        app: sparta-wl-screening
    spec:
      containers:
        - name: sparta-wl-screening-container
          image: gcr.io/blackswan-161813/sparta-wl-screening:1.0.5
          ports:
            - containerPort: 8000
              protocol: "TCP"
          
          resources:
            requests:
              memory: "256Mi"
              cpu: "250m"
            limits:
              memory: "1024Mi"
              cpu: "500m"
```

6. In services.yml, declare your service
```yaml
apiVersion: v1
kind: Service
metadata:
  name: sparta-wl-screening-service
  labels:
    app: sparta-wl-screening
spec:
  type: NodePort
  selector:
    app: sparta-wl-screening
  ports:
    - port: 80
      targetPort: 8000
      protocol: TCP
```


7. Follow the [README](https://bitbucket.org/bst_element/gcloud-deployments/src/master/gcp_dep/README) to apply changes on Kube 
```bash
$kubectl apply -f <sparta-wl-deployment.yml>
$kubectl apply -f <sparta-wl-service.yml>
$kubectl apply -f <sparta-wl-ingress.yml>
```
Please refer the name from Container

8. Check on the CI server to see the status
```bash
$kubectl get pods
```
```bash
sparta-wl-screening-deployment-5d879b8ddd-nfjrt   1/1     Running   0          3h40m
```
## Payload

```html
http://wl-screening.sparta.xara.ai/v1/watchlist-screening/person
http://wl-screening.sparta.xara.ai/v1/watchlist-screening/organization
```

```json
{
    "confidence": 0.9,
    "entities": [
        {
            "name": "",
            "entity_type": "Person or Organization"
        },
        
         {
            "name": "",
            "entity_type": "Person or Organization"
        }
    ]
}
```


