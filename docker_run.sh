#!/bin/bash

#Replace image URL in TAG
ROOT=$PWD
TAG=sparta-screen-test:latest
NAME=sparta-screen-test
## PRINT Codes


# Build the image if we need to
#docker build --tag $TAG $PWD
#printf "${GREEN}Docker image build successful${NC}\n"
#printf "${GREEN}Stopping Running Containers${NC}\n"
# Check if a container with the same image tag, already exists

CONTAINER=$(docker ps -aq -f name=$NAME)
if [ ! -z "$CONTAINER" ]; then
  # Check if the container is currently running
  STATUS=$(docker inspect -f {{.State.Status}} $CONTAINER)
  if [ "$STATUS" == "running" ]; then
    docker stop $CONTAINER
  fi

  # Remove the old container
  docker rm --force $CONTAINER
fi
printf "${GREEN}Starting the Container${NC}\n"

#define WL_DATASOURCE_BUCKET
docker run --name $NAME -it -d -p 80:8000 \
    -e "container=docker" \
    -e "WL_DATASOURCE_BUCKET=wl_datasource_bst" \
    --privileged=true \
    --restart=always \
     $TAG

printf "${GREEN}Starting Success, you can access the docker container by {docker exec -it $NAME /bin/bash}${NC}\n"
