include $(env).sh


TAG=$(project_name)/$(container_name):$(version)

all: stash_changes connect_cluster build_image tag push create_deployment_yml deploy

build_and_push: build_image tag push

build_image:
	docker build  -t $(container_name):$(version) . --label version=$(version)

tag:
	docker tag "$(container_name):$(version)" "gcr.io/$(project_name)/$(container_name):$(version)"
	
clair:
	/opt/clair/clair-scanner --clair="http://0.0.0.0:6060"  --ip="172.17.0.1" --report="sparta-wl-screening.json" gcr.io/$(PROJECT)/$(NAME):$(VERSION)

push:
	docker push  "gcr.io/$(project_name)/$(container_name):$(version)"

create_deployment_yml:
	sudo sed -i "s/{{image}}/gcr.io\/$(project_name)\/$(container_name):$(version)/g" sparta-wl-screening-deployment.yml
	sudo sed -i "s/{{replicas}}/$(replicas_count)/g" sparta-wl-screening-deployment.yml
	sudo sed -i "s/{{container_name}}/$(container_name)/g" sparta-wl-screening-deployment.yml
	sudo sed -i "s/{{container_port}}/$(container_port)/g" sparta-wl-screening-deployment.yml

create_service_yml:
	sudo sed -i "s/{{service_name}}/$(container_name)/g" sparta-wl-screening-service.yml

deploy_service: create_service_yml
	kubectl apply -f sparta-wl-screening-service.yml


deploy:
	kubectl apply -f sparta-wl-screening-deployment.yml

connect_cluster:
	gcloud container clusters get-credentials $(cluster_name) --region $(region) --project $(project_name)

unit_test:
	echo "test"

stash_changes:
	sudo git stash
integration_test:
#	echo gcr.io/$(project_name)/$(container_name):$(version)
	echo $(TAG)

#############################################dev3#######################
build_dev3:
	docker build --tag gcr.io/sparta-dev3/sparta-wl-screening-v2:$(VERSION) .

clair_dev3:
	/opt/clair/clair-scanner --clair="http://0.0.0.0:6060"  --ip="172.17.0.1" --report="clair.json" gcr.io/sparta-dev3/sparta-wl-screening-v2:$(VERSION)

push_dev3:
	docker push gcr.io/sparta-dev3/sparta-wl-screening-v2:$(VERSION)

connect_dev3:
	gcloud container clusters get-credentials sparta-dev3-backend-cluster --region us-east1 --project sparta-dev3

replace_dev3:
	sed -i "s/{{VERSION}}/$(VERSION)/g" dev3k8s/deployment.yaml

deploy_dev3:
	kubectl apply -f dev3k8s/

#############################dev3#########################

.PHONY: build_packages
