FROM python:3.7.6-slim
RUN apt-get update -y
RUN apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev -y
RUN apt-get install git -y
RUN apt-get install python3.7 python3.7-dev -y
RUN apt-get install python3-pip  -y
RUN apt-get install libomp-dev -y
RUN apt-get install libopenblas-dev -y
RUN  python3.7 -m pip install --upgrade pip
RUN  python3.7 -m pip install --upgrade setuptools

##  SETTING ENV ##
ADD requirements.txt /root
RUN  python3.7 -m pip install -r /root/requirements.txt

#installing attribute-matcher
ADD attribute-matcher.zip /root
RUN  python3.7 -m pip install /root/attribute-matcher.zip

#installing entity-matcher-index
ADD entity-matcher-index.zip /root
RUN  python3.7 -m pip install /root/entity-matcher-index.zip

ADD DJW-Index-201912052200 /root/src/module/screening

#building GCR dir
RUN mkdir -p /bst/app/src
COPY src  /bst/app/src
#RUN apt install locales -y
RUN apt-get install curl -y
WORKDIR /bst/app/src
CMD ["python3.7", "run.py", "run:app","--bind", "0.0.0.0:8000"]
#ENTRYPOINT tail -f /var/log/*
