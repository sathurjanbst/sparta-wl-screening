import os.path
import logging as logger

try:
    from src.module.screening.gcs_handler import *
except:
    from module.screening.gcs_handler import *


def check_index_file(index_file_name):
    index_path = index_file_name
    if os.path.isfile(index_path):
        logger.info("Index file name in {} already exists".format(index_path))
        flag = True
    else:
        flag = False
        logger.info(index_file_name + " doesn't exist - downloading from GCS")

    return flag


def delete_ext_file(directory='/bst/app/src/module/screening/'):
    try:
        test = os.listdir(directory)
        for item in test:
            if item.startswith("DJW-Index-"):
                os.remove(os.path.join(directory, item))

        logger.info("Removed ext index")
    except Exception as e:
        logger.info("Removing failed " + str(e))


def delete_source_files(directory='/bst/app/src/module/screening/'):
    try:
        test = os.listdir(directory)
        for item in test:
            if item.endswith(".xml"):
                os.remove(os.path.join(directory, item))

        logger.info("Removed indexed source files")

    except Exception as e:
        logger.info("Removing failed " + str(e))


def download_index():
    pass


def check_and_download(bucket_name, source_blob_name, destination_file_name):
    if check_index_file(destination_file_name):
        logger.info('index check completed. Downloading...')
        read_index(bucket_name, source_blob_name, destination_file_name)
        logger.info('Index download completed.')

    else:
        logger.error('Error in downloading index file')
