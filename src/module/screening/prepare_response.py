import json
import logging as logger
import requests


class Filter(object):

    def __init__(self, input_data, max_responses, entity_type):
        self.input_data = input_data
        self.max_responses = max_responses
        self.entity_type = entity_type

    def filter(self):
        data = json.loads(self.input_data)
        # data = self.input_data
        filtered_hits = []
        ids, hits = self.get_ids(data)

        if self.entity_type == 'PERSON':
            matches = self.create_id_group(ids, hits)
            sorted_groups = self.sort_id_groups(matches, ids)
            filtered_hits = sorted_groups[:(int(self.max_responses))] if self.max_responses > 0 else sorted_groups[:20]

        if self.entity_type == 'ORG':
            primary_matches, other_matches = self.get_primary_name_groups(ids, hits)
            merged_desc_groups = self.merge_descriptions(ids, primary_matches, other_matches)
            _sorted_groups = self.sort_org_id_groups(ids, merged_desc_groups)
            filtered_hits = _sorted_groups[:(int(self.max_responses))] if self.max_responses > 0 else _sorted_groups[:20]

        return filtered_hits

    def get_ids(self, resp):
        idArr = []
        vals = []
        for data in resp:
            vals.append(data)
            idArr.append(data['basic_info']['id'])
            idArr = list(dict.fromkeys(idArr))
        return idArr, vals

    # def filter(self, ids, hits):
    #     hit_list = []
    #     temp = []
    #     for id in ids:
    #         print(id)
    #
    #     return hit_list

    def create_id_group(self, id_list, hits_list):
        id_groups = []

        for id in id_list:
            # print(id)
            hits = []
            for hit in hits_list:

                if id == hit['basic_info']['id']:
                    hits.append(hit)

            group = {
                id: hits
            }

            id_groups.append(group)
        return id_groups

    def sort_id_groups(self, id_groups, ids):
        sorted_id_groups = []
        for id_group, _id in zip(id_groups, ids):
            sorted_id_groups.append(sorted(id_group[_id], key=lambda i: i['confidence'], reverse=True)[0])

        return sorted_id_groups

    def get_primary_name_groups(self, id_list, hits_list):

        primary_name_group = []
        other_name_group = []

        for id in id_list:
            hits = []
            _hits = []
            for hit in hits_list:

                if id == hit['basic_info']['id']:
                    if hit['basic_info']['name_type'] == 'primary':
                        hits.append(hit)

                    else:
                        _hits.append(hit)

            _group = {
                id: _hits
            }

            other_name_group.append(_group)

            group = {
                id: hits
            }

            primary_name_group.append(group)

        return primary_name_group, other_name_group

    def merge_descriptions(self, ids, primary_name_group, other_name_group):
        groups_with_merged_descs = []

        for _id, _primary_group in zip(ids, primary_name_group):
            descs = map(lambda i: i['basic_info']['descriptions'], _primary_group[_id])
            _descs = (list([j for i in descs for j in i]))

            for item in _primary_group[_id]:
                item['basic_info']['descriptions'] = _descs
                entries = []

                for desc in zip(_descs):
                    entries.append({
                        "watchlist_id": item['basic_info']['watchlist_id'],
                        "source": "DJW Index",
                        "classification": desc[0],
                    })

                item['entries'] = entries

                groups_with_merged_descs.append(item)

        for _id, _other_name in zip(ids, other_name_group):
            for item in _other_name[_id]:
                # print(item['basic_info']['descriptions'])

                entries = []

                for other_desc in item['basic_info']['descriptions']:
                    entries.append({
                        "watchlist_id": item['basic_info']['watchlist_id'],
                        "source": "DJW Index",
                        "classification": self.check_list(other_desc),
                    })

                item['entries'] = entries

                groups_with_merged_descs.append(item)

        return groups_with_merged_descs

    def sort_org_id_groups(self, ids, id_groups):
        sorted_by_confidence = []
        _id_groups = self.create_id_group(ids, id_groups)
        sorted_id_groups = self.sort_id_groups(_id_groups, ids)
        sorted_by_confidence.append(sorted(sorted_id_groups, key=lambda i: i['confidence'], reverse=True))
        return sorted_by_confidence[0]

    def check_list(self, desc_list):
        logger.info(desc_list)
        if isinstance(desc_list, list):
            return desc_list[0]
        else:
            return desc_list

    def merge_results(self, hits):
        pass

# with open('test.json') as f:
#     data = json.load(f)
#     # print(data)
#
# fil = Filter(data, 0, "ORG")
# # print(fil.main())
# out = fil.main()
#
# with open('out_file.json', 'w') as file:
#     file.write(json.dumps(out))
