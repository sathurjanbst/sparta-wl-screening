import json
import os
import logging
from datetime import datetime
import boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def screen_entity(event, context):
    if not event:
        return get_response(json.dumps(output_error(4)), get_error_code(4))

    if "body" in event:
        try:
            input = json.loads(event['body'])
        except:
            return get_response(json.dumps(output_error(4)), get_error_code(4))
    else:
        input = event

    process_response, status_code = process_entities(input)

    return get_response(json.dumps(process_response), status_code)


def process_entities():
    pass


def get_response(response: str, status_code=200):
    return {
        'body': response,
        'headers': {
            'Content-Type': 'application/json',
            "Access-Control-Allow-Origin": "*",
            "developer": "BST-CTG",
            "Access-Control-Allow-Headers": "Content-Type",
            "Access-Control-Allow-Methods": "*"
        },
        'statusCode': status_code
    }


def get_error_code(err_code):
    err_dict = {
        1: 401,
        2: 402,
        3: 403,
        4: 404
    }

    return err_dict.get(err_code, 200)


def get_error_msg(err_code, err_info=''):
    err_dict = {
        1: "No any watchlists found for given 'Watchlist' condition.",
        2: "No any entities found in the request.",
        3: "Error occured in the matching process.",
        4: "Invalid request."
    }

    return (err_dict.get(err_code, "Unhandled exception occurred")) + err_info


def output_error(err_code, err_info=''):
    err_msg = get_error_msg(err_code, err_info)

    status = {
        "status": "failed",
        "message": err_msg
    }

    return status
