import json

def get_error_msg(err_code, err_info=''):
    err_dict = {
        404: "Not Found",
        500: "Internal Server Error",
        502: "502 Internal Server Error",
        2: "Field 'name' is missing",
        3: "Field 'entity_id' is missing "
    }

    return (err_dict.get(err_code, "Unhandled exception occurred")) + str(err_info)


def output_error(err_code, err_info):
    err_msg = get_error_msg(err_code, err_info)

    status = {
        "Type": "Screening Error",
        "message": err_msg + " " + str(err_info)
    }

    return json.dumps(status, indent=4)


