from entity_matcher_index.index import EntityMatcherIndex, DEFAULT_CONFIGS
import time
import datetime
from typing import Dict, List
import logging as logger

try:
    from src.module.screening.gcs_handler import *
    from src.module.screening.prepare_response import *
    from src.module.screening.file_handlers.file_handler import *
except:
    from module.screening.gcs_handler import *
    from module.screening.prepare_response import *
    from module.screening.file_handlers.file_handler import *

try:
    bucket_name = os.environ['WL_DATASOURCE_BUCKET']
except:
    bucket_name = 'wl_datasource'

print(bucket_name)

index_name_prefix = 'index/'
data_source_prefix = 'xml/'


class OrgEntityMatcher(object):

    def __init__(self, index):
        self.index = index

    def start_org_screening(self, query_entities: dict, threshold: float, max_responses: int) -> List[Dict[str, str]]:

        if self.index:
            matches = self.get_matches(self.index, query_entities, threshold=threshold)
            # logger.info(matches)
            logger.info(max_responses)
            responses = self.prepare_org_matches(matches, query_entities, max_responses)
            logger.info(len(responses))
            logger.info(responses)

            return responses
        else:
            logger.error("Matching Error")

    def define_algorithm(self, m):
        algo = m[2]['name']['matching_strategy']
        return algo

    def filter_by_primary_name(self, m):
        name_type = m[1]['name_type']

        return name_type == 'primary'

    def prepare_org_matches(self, matches, query_entities, max_responses) -> List:
        results = []

        for i, match_row in enumerate(matches):
            hits = []
            # tr_match_rows = match_row[:(int(max_responses))] if max_responses > 0 else match_row[:20]

            for m in match_row:
                # if self.filter_by_primary_name(m):
                logger.info(m)
                hits.append({
                    "value": m[1]['name'],
                    "confidence": m[0],
                    "algorithm": self.define_algorithm(m),
                    "basic_info": m[1],
                    "attributes": self.parse_attributes(m),
                    "entries": self.get_entries(m)
                })
                # hits.append(m)
            _filter = Filter(input_data=json.dumps(hits), max_responses=max_responses, entity_type='ORG')

            results.append({
                "name": query_entities[i]["name"],
                "entity_id": query_entities[i]['entity_id'],
                "dor": query_entities[i]['date_of_registration'],
                "screening_method": "RT",
                "creation_time": self.get_creation_time(),
                "type": query_entities[i]["entity_type"],
                "hits": _filter.filter()
            })

        return results

    def get_entries(self, data):
        items = []
        classifications, wl_id = self.get_classifcation(data)
        for item in classifications:
            items.append({
                "watchlist_id": wl_id,
                "source": "DJW Index",
                "classification": item,
            })

        return items

    def parse_attributes(self, data_set):
        entries = [
            {
                "attribute_name": "name",
                "attribute_value": data_set[1]['name'],
                "confidence": data_set[2]['name']['similarity'],
                "matching_strategy": data_set[2]['name']['matching_strategy']
            },
            {
                "attribute_name": "date_of_registration",
                "attribute_value": data_set[1]['date_of_registration'],
                "confidence": data_set[2]['date_of_registration']['similarity'],
                "matcher": "date matcher"
            },
            {
                "attribute_name": "place_of_registration",
                "attribute_value": data_set[1]['place_of_registration'],
                "confidence": data_set[2]['place_of_registration']['similarity'],
                "matcher": "Country matcher"
            }

        ]

        return entries

    def get_classifcation(self, data):
        try:
            watchlist_id = data[1]['watchlist_id']
        except:
            watchlist_id = ''
        descs = data[1]['descriptions']

        return descs, watchlist_id

    def get_creation_time(self):
        now = datetime.datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        return dt_string

    def get_index(self, watchlist: dict, attribute_weights_list: dict, index_type: str):
        start = time.time()
        index = EntityMatcherIndex(index_type=index_type,
                                   configs=attribute_weights_list if attribute_weights_list else DEFAULT_CONFIGS)
        init_finish = time.time()
        print(f'init time: {(init_finish - start) // 60:.0f}min {(init_finish - start) % 60:.0f}s')

        index.index(watchlist, num_threads=1)
        index_finish = time.time()
        print(f'index time: {(index_finish - init_finish) // 60:.0f}min {(index_finish - init_finish) % 60:.0f}s')

        return index

    def get_matches(self, index, query_entities, threshold):
        start = time.time()
        logger.error(query_entities)
        matches = index.match(query_entities, threshold=threshold)
        match_finish = time.time()
        print(f'match time: {(match_finish - start) // 60:.0f}min {(match_finish - start) % 60:.0f}s')

        return matches

    def save_org_index(self, watchlist: dict, attribute_weights_list, index_type: str, wl_id: str):
        index = self.get_index(watchlist, attribute_weights_list, index_type)
        index_name = 'DJW-Index-' + index_type + wl_id
        destination_index_name = 'index/' + index_name
        index.save(index_name)

        upload_index(bucket_name, destination_index_name, index_name)

