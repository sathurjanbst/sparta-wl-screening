import requests
import json
from multiprocessing import Process
import time

# BASE_URI = 'http://wl-screening.sparta.xara.ai/v1/watchlist-screening/person'
BASE_URI = 'http://wl-screening.sparta.xara.ai/'

headers = {
    'Content-Type': 'application/json'
}

names = [
    {"name": "Rita", "date_of_birth": ""},
    {"name": "Bin", "date_of_birth": ""},
    {"name": "fahda", "date_of_birth": ""},
    {"name": "tan", "date_of_birth": ""},
    {"name": "dil", "date_of_birth": ""},
    {"name": "osama", "date_of_birth": ""}
]


def print_func():
    data = get_req()
    if data is not None:
        try:
            select_data = get_req()
            print(select_data)
        except Exception as e:
            print(e)


def get_req():
    url = BASE_URI
    # req_body = {
    #     "confidence": 0.1,
    #     "entities": [
    #         {
    #             "entity_id": "1",
    #             "name": name,
    #             "date_of_birth": dob,
    #             "entity_type": "person",
    #             "place_of_birth": "",
    #             "isAlive": "Yes"
    #         }
    #     ]
    # }

    st = time.time()
    flag = True
    while flag:
        # response = requests.post(url, json=req_body, headers=headers)
        response = requests.get(url)

        if response.status_code is 200:
            flag = False
            data = response.text
            et = time.time() - st
            print('----- > Time taken for ', url, et)
            return data
        else:
            print(url, ' error')
    et = time.time() - st
    print('----- > Time taken for ', url, et)
    return None


if __name__ == "__main__":
    start_time = time.time()
    procs = []

    for i in range(0,100):
        # proc = Process(target=print_func, args=(
        #     name['name'],
        #     name['date_of_birth']
        #     )
        # )
        proc = Process(target=print_func)
        procs.append(proc)
        proc.start()

    for proc in procs:
        proc.join()

    elapsed_time = time.time() - start_time
    print('----- > Total Time taken ', elapsed_time)
