from src.module.screening.match_entities import EntityMatcher, DEFAULT_CONFIGS
# from src.module.entity_matcher_index.index import En
from src.module.screening.prepare_screening_entities import PrepareEntity
import json

em = EntityMatcher()

wl = em.load_local_json()

# q = [{'id': 1, 'name': 'john', 'date_of_birth': '', 'place_of_birth': 'IND', 'entity_type':'person'}]
q = {
        "confidence": 0.9,
        "entities":
            [
                {
                    "name": "Smith",
                    "date_of_birth": "",
                    "entity_type": "person",
                    "place_of_birth": "IND"

                },
                {
                    "name": "John",
                    "entity_type": "org",
                    "date_of_birth": "",
                    "place_of_birth": "IND"
                }
            ]

    }


pe = PrepareEntity()
proccessed_payload = pe.prepare_screening_json(q)
em.start_screening(wl, proccessed_payload, "PERSON", DEFAULT_CONFIGS, 0)
