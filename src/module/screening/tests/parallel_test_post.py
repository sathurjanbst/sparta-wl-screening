import requests
import json
from multiprocessing import Process
import time

BASE_URI = 'http://wl-screening.sparta.xara.ai/v1/watchlist-screening/person'
# BASE_URI = 'http://wl-screening-test.sparta.xara.ai/v1/watchlist-screening/person'
# BASE_URI = 'http://0.0.0.0:8000/v1/watchlist-screening/person'


headers = {
    'Content-Type': 'application/json'
}

names = [
    {"name": "Thierry Henry", "date_of_birth": ""},
    {"name": "Saddam Hussein", "date_of_birth": ""},
    {"name": "Osama Bin Laden", "date_of_birth": ""},
    {"name": "Bill Gates", "date_of_birth": ""},
    {"name": "Donald Trump", "date_of_birth": ""}

]


def print_func(name='rita', dob=''):
    data = get_req(name, dob)
    if data is not None:
        try:
            select_data = get_req(name, dob)
            print(select_data)
        except Exception as e:
            print(e)

def get_req(name, dob):
    url = BASE_URI
    req_body = {
        "confidence": 0.1,
        "entities": [
            {
                "entity_id": "1",
                "name": name,
                "date_of_birth": dob,
                "entity_type": "person",
                "place_of_birth": "",
                "isAlive": "Yes"
            }
        ]
    }

    st = time.time()
    flag = True
    while flag:
        response = requests.post(url, json=req_body, headers=headers)
        if response.status_code is 200:
            flag = False
            # data = json.loads(response.text)
            data = ''
            print(response.status_code)
            et = time.time() - st
            print('----- > Time taken for ', name, url, et)
            return data
        else:
            print(response.status_code)
            print(response.text, ' error')
    et = time.time() - st
    print('----- > Time taken for ', name, url, et)
    return None


if __name__ == "__main__":
    start_time = time.time()
    procs = []

    for j in range(0,3):
        for name in names:
            print(name['name'])
            proc = Process(target=print_func, args=(
                name['name'],
                name['date_of_birth']
                )
            )
            procs.append(proc)
            proc.start()

    for proc in procs:
        proc.join()

    elapsed_time = time.time() - start_time
    print('----- > Total Time taken ', elapsed_time)