import sys
import logging
import json

logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s', level=logging.INFO, stream=sys.stdout)
logger = logging.getLogger()

from entity_matcher_index import EntityMatcherIndex

if __name__ == '__main__':
    with open('screen_list_2.json', 'r') as f:
        wl = json.load(f)

    query_entities = [
        # {'entity_id': '1', 'name': 'Al Fahda', 'date_of_birth': '1975.10.21', 'entity_type': 'person', 'place_of_birth': 'USA', 'isAlive': 'Yes', 'id': '98d545618475bdb099f51cd235e8d9c4'},
        # {"entity_id": "1", "name": "Smith", "date_of_birth": "", "entity_type": "person", "place_of_birth": "USA", "deceased": "Yes"},
        # {"entity_id": "1", "name": "Al Fahda", "date_of_birth": "", "entity_type": "person", "place_of_birth": "USA", "deceased": "Yes"},
        # {"entity_id": "1", "name": "Jean-Edmond", "date_of_birth": "", "entity_type": "person", "place_of_birth": "ZAIRE", "deceased": "Yes"},
        # {"entity_id": "5", "name": "Rita Siqueira Campos", "date_of_birth": "", "entity_type": "person", "place_of_birth": "BRAZ", "deceased": "No"},
        # {"entity_id": "6", "name": "Frieda", "date_of_birth": "", "entity_type": "person", "place_of_birth": "BELG", "deceased": "No"}
        {"name": "Bazza Al Saud"},
        {"name": "Bint Al Fiqri"},
        {"name": "Saud Bin Abdul Aziz Bin Abdul Rahman Al Saud"},
    ]

    index = EntityMatcherIndex(index_type='PERSON')
    index.index(wl)
    index.save('9M_index_person')

    # matches = index.match(query_entities, threshold=0.1, num_threads=1)
    # for q, hits in zip(query_entities, matches):
    #     print(
    #         f'Query: name="{q["name"]}" | date_of_birth="{q.get("date_of_birth", "")}" | place_of_birth="{q.get("place_of_birth", "")}"')
    #     print('Hits:')
    #     for h in hits[:3]:
    #         print(f'\t{h[0]}, "{h[1]["name"]}"')
    #     print()
    #     print('-' * 100)
    #     print()
