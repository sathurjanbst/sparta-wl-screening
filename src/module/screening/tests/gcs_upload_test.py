from google.cloud import storage
import os
import logging as logger

storage_client = storage.Client.from_service_account_json('../queue_handlers/service_account.json')


def explicit():
    # storage_client = storage.Client.from_service_account_json('service_account.json')

    # Make an authenticated API request
    buckets = list(storage_client.list_buckets())
    print(buckets)


bucket_name = "wl_datasource"
source_file_name = "/src/index_index_DJW-Index-201912052200"
destination_blob_name = "index/DJW-Index-201912052200"


def upload_blob():
    """Uploads a file to the bucket."""

    storage_client = storage.Client.from_service_account_json('../queue_handlers/service_account.json')
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print(
        "File {} uploaded to {}.".format(
            source_file_name, destination_blob_name
        )
    )


filename = 'DJW-Index-201912052200'


def read_index(bucket_name, source_blob_name, destination_file_name):
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(destination_file_name)

    print(
        "Blob {} downloaded to {}.".format(
            source_blob_name, destination_file_name
        )
    )


# upload_blob()
# read_index(bucket_name, destination_blob_name, filename)

def download_xml_files(bucket_name):
    xml_download_path = '/home/vampire/Desktop/BST/SPARTA/sparta-wl-screening/src/module/screening/tests'
    blobs = storage_client.list_blobs(bucket_name, prefix='xml/')
    blobs_names_list = []
    blobs_list = []

    for blob in blobs:
        # blob.download_to_filename(xml_download_path + str(blob.name).replace('xml/',''))
        blobs_list.append(blob.name)

    print(blobs_list)
    return blobs_list


def classify_filenames(filenames):
    person_files, org_files = [], []

    for file in filenames:
        org_files.append(str(file).replace('xml/', ''))

        if str(file).__contains__('PFA2'):
            person_files.append(str(file).replace('xml/', ''))

    return person_files, org_files


import datetime


def check_timestamp(bucket_name, gcs_path, local_path):
    d1 = get_gcs_file_timestamp(bucket_name, gcs_path)
    d2 = get_local_file_timestamp(local_path)

    gcs_timestamp = datetime.datetime.strptime(str(d1).split('.')[0], "%Y-%m-%d %H:%M:%S")
    local_timestamp = datetime.datetime.strptime(str(d2), "%Y-%m-%d %H:%M:%S")

    print(gcs_timestamp, local_timestamp)

    if max(gcs_timestamp, local_timestamp) == gcs_timestamp:
        print(max(gcs_timestamp, local_timestamp))
        return True
    else:
        print(max(gcs_timestamp, local_timestamp))
        return False


def get_gcs_file_timestamp(bucket_name, blob_name):
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.get_blob(blob_name)

    logger.info("Updated: {}".format(blob.updated))

    return blob.updated


def get_local_file_timestamp(local_path):
    timestamp = os.path.getmtime(local_path)

    return datetime.datetime.fromtimestamp(
        float(timestamp)
    ).strftime('%Y-%m-%d %H:%M:%S')


# get_local_file_timestamp('/home/vampire/Desktop/BST/SPARTA/sparta-wl-screening/src/module/screening/tests/gcs_upload_test.py')
# get_gcs_file_timestamp(bucket_name, "index/DJW-Index-201912052200")

# print(check_timestamp(bucket_name, "index/DJW-Index-201912052200",
#                       '/home/vampire/Desktop/BST/SPARTA/sparta-wl-screening/src/module/screening/DJW-Index-201912052200')
#       )

ts = datetime.datetime.now()

print(datetime.datetime.now().strftime('%Y%m%d%H%M%S'))




