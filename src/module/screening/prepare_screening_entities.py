import hashlib
import json
import logging as logger
import module.screening.error_handlers.error_handler as eh


class PrepareEntity:
    def get_hashed_id(self, text: str) -> str:
        text = text.encode('utf-8')
        m = hashlib.md5()
        m.update(text)
        return m.hexdigest()

    def load_local_json(self):
        with open('tests/sample_payload.json', 'r') as f:
            return json.load(f)

    def list_keys(self, json_obj):
        keys = [key for key in json_obj[0].keys()]
        return keys

    def prepare_screening_json(self, input_json: dict):
        processed_payload = []
        keys_list = self.list_keys(input_json['entities'])
        conf_level = input_json['confidence']

        try:
            max_responses = input_json['max_responses']
        except Exception as e:
            logger.info(e)
            max_responses = 0

        error = ''

        for screen_item in input_json['entities']:
            screen_item['id'] = self.get_hashed_id(screen_item['name'])

            if 'date_of_birth' not in keys_list:
                screen_item['date_of_birth'] = ''

            if 'place_of_birth' not in keys_list:
                screen_item['place_of_birth'] = ''

            if 'entity_id' not in keys_list:
                error = eh.output_error(3)

            if 'name' not in keys_list:
                error = eh.output_error(2)

            if 'entity_type' not in keys_list:
                error = eh.output_error(1)

            processed_payload.append(screen_item)
        return processed_payload, conf_level, error, max_responses


class PrepareOrgEntity:
    def get_hashed_id(self, text: str) -> str:
        text = text.encode('utf-8')
        m = hashlib.md5()
        m.update(text)
        return m.hexdigest()

    def load_local_json(self):
        with open('tests/sample_payload.json', 'r') as f:
            return json.load(f)

    def list_keys(self, json_obj):
        keys = [key for key in json_obj[0].keys()]
        return keys

    def prepare_screening_json(self, input_json: dict):
        processed_payload = []
        keys_list = self.list_keys(input_json['entities'])
        conf_level = input_json['confidence']

        try:
            max_responses = input_json['max_responses']
        except Exception as e:
            logger.info(e)
            max_responses = 0

        error = ''

        for screen_item in input_json['entities']:
            screen_item['id'] = self.get_hashed_id(screen_item['name'])

            if 'date_of_registration' not in keys_list:
                screen_item['date_of_registration'] = ''

            if 'place_of_registration' not in keys_list:
                screen_item['place_of_registration'] = ''

            if 'name' not in keys_list:
                error = eh.output_error(2)

            if 'entity_type' not in keys_list:
                error = eh.output_error(1)

            processed_payload.append(screen_item)
        return processed_payload, conf_level, error, max_responses
