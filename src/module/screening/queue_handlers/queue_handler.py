from google.cloud import pubsub_v1
from src.module.screening.queue_handlers.settings import *
import time
project_id = PROJECT_ID
response_queue = RESPONSE_QUEUE
request_queue = REQUEST_QUEUE
subscription_name = REQUEST_SUB


try:
    subscriber = pubsub_v1.SubscriberClient.from_service_account_file('service_account.json')
except:
    subscriber = pubsub_v1.SubscriberClient()


subscription_path = subscriber.subscription_path(
    project_id, subscription_name
)


def callback(message):
    print("Received message: {}".format(message))
    message.ack()
    get_message(message)


def get_message(message):
    return message


streaming_pull_future = subscriber.subscribe(
    subscription_path, callback=callback
)

print("Listening for messages on {}..\n".format(subscription_path))

with subscriber:
    try:
        streaming_pull_future.result(timeout=180)
    except Exception as e:
        print(e)
        streaming_pull_future.cancel()
