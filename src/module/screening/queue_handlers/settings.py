import os

try:
    PROJECT_ID = 'blackswan-161813'
    RESPONSE_QUEUE = 'wl-response-queue'
    REQUEST_QUEUE = 'wl-request-queue'
    REQUEST_SUB = 'wl-request-subscription'
    RESPONSE_SUB = ''
except:
    PROJECT_ID = os.environ['PROJECT_ID']
    RESPONSE_QUEUE = os.environ['RESPONSE_QUEUE']
    REQUEST_QUEUE = os.environ['REQUEST_QUEUE']
    RESPONSE_SUB = os.environ['RESPONSE_SUB']
    REQUEST_SUB = os.environ['REQUEST_SUB']
