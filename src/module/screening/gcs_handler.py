from google.cloud import storage
import logging as logger
import os
import datetime
import requests

try:
    storage_client = storage.Client.from_service_account_json('service_account.json')
except Exception as e:
    storage_client = storage.Client()
    print(e)


def explicit():
    buckets = list(storage_client.list_buckets())
    print(buckets)


def list_indices(bucket_name, prefix):
    blobs = storage_client.list_blobs(bucket_name, prefix=prefix)
    org_indices = []
    person_indices = []

    for blob in blobs:
        if str(blob.name).__contains__('DJW-Index-Org'):
            org_indices.append(str(blob.name).replace(str(prefix), ''))

        if str(blob.name).__contains__('DJW-Index-Person'):
            person_indices.append(str(blob.name).replace(str(prefix), ''))

    if len(org_indices) > 0 or len(person_indices) > 0:
        logger.info('Indices found on {} are Person - {} , Org - {}.'.format(prefix, person_indices, org_indices))
        return str(person_indices[0]) if len(person_indices) > 0 else None,  str(org_indices[0]) if len(org_indices) > 0 else None
    else:
        logger.info('No indices found on {} .'.format(prefix))
        return None,  None


def upload_index(bucket_name, destination_blob_name, source_file_name):
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    logger.info(
        "Index File {} saved into {}.".format(
            source_file_name, destination_blob_name
        )
    )


def read_index(bucket_name, source_blob_name, destination_file_name):
    download_path = '/bst/app/src/module/screening/' + destination_file_name
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(download_path)

    logger.info(
        "Index file {} successfully downloaded to {}.".format(
            source_blob_name, download_path
        )
    )


def download_xml_files(bucket_name):
    xml_download_path = '/bst/app/src/data/'
    blobs = storage_client.list_blobs(bucket_name, prefix='xml/')
    blobs_list = []

    for blob in blobs:
        blob.download_to_filename(xml_download_path + str(blob.name).replace('xml/', ''))
        logger.info(str(blob.name).replace('xml/', '') + " Download completed.")
        blobs_list.append(str(blob.name).replace('xml/', ''))

    logger.info(blobs_list)
    return blobs_list


def check_timestamp(bucket_name, gcs_path, local_path):
    d1 = get_gcs_file_timestamp(bucket_name, gcs_path)
    d2 = get_local_file_timestamp(local_path)

    gcs_timestamp = datetime.datetime.strptime(str(d1).split('.')[0], "%Y-%m-%d %H:%M:%S")
    local_timestamp = datetime.datetime.strptime(str(d2), "%Y-%m-%d %H:%M:%S")

    logger.info("GCS Timestamp: {} | Docker Timestamp: {}".format(gcs_timestamp, local_timestamp))

    if max(gcs_timestamp, local_timestamp) == gcs_timestamp:
        return True
    else:
        return False


def get_gcs_file_timestamp(bucket_name, blob_name):
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.get_blob(blob_name)

    logger.info("Updated: {}".format(blob.updated))

    return blob.updated


def get_local_file_timestamp(local_path):
    timestamp = os.path.getmtime(local_path)

    return datetime.datetime.fromtimestamp(
        float(timestamp)
    ).strftime('%Y-%m-%d %H:%M:%S')


def create_archive_folder_name():
    return datetime.datetime.now().strftime('%Y%m%d%H%M%S')


def archive_files(bucket_name, source_blob_name, destination_blob_name):
    archive(bucket_name, source_blob_name, bucket_name, destination_blob_name)


def archive(bucket_name, blob_name, destination_bucket_name, destination_blob_name):
    source_bucket = storage_client.bucket(bucket_name)
    source_blob = source_bucket.blob(blob_name)
    destination_bucket = storage_client.bucket(destination_bucket_name)

    blob_copy = source_bucket.copy_blob(
        source_blob, destination_bucket, destination_blob_name
    )

    logger.info(
        "Index {} in bucket {} successfully archived {} .".format(
            source_blob.name,
            source_bucket.name,
            blob_copy.name,
        )
    )


def list_items(bucket_name, prefix):
    blobs = storage_client.list_blobs(bucket_name, prefix=prefix)
    blobs_list = []

    for blob in blobs:
        blobs_list.append(str(blob.name).replace(str(prefix), ''))

    return blobs_list
