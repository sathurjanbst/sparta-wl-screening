import json


class ProccessInput:

    def process_name(self, input_json):
        names_and_entities = []
        for item in input_json['entities']:
            proceed_item = {}

            proceed_item['name'] = item['name']
            proceed_item['entity_type'] = item['entity_type']

            names_and_entities.append(proceed_item)

        return names_and_entities
