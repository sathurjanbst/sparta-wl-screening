from flask import Flask, render_template, request, session, redirect, send_from_directory
from flask import Response, request
import json
import hashlib
from flask import Blueprint
from flask import jsonify
import logging as logger
from module.screening.match_entities import EntityMatcher
from module.screening.match_org_entities import OrgEntityMatcher

from entity_matcher_index.index import DEFAULT_CONFIGS, EntityMatcherIndex
from module.screening.prepare_screening_entities import PrepareEntity, PrepareOrgEntity
import module.screening.error_handlers.error_handler as eh
from module.screening.get_names_from_query import ProccessInput

try:
    from src.module.screening.file_handlers.file_handler import *
    from src.module.screening.gcs_handler import *
    from src.module.screening.prepare_response import Filter
except:
    from module.screening.file_handlers.file_handler import *
    from module.screening.gcs_handler import *
    from module.screening.prepare_response import Filter

# Define WSGI object
app = Flask(__name__)

# Configurations
app.config.from_object('config')
watchlist = Blueprint('watchlist', __name__)

# Configurations
app.config.from_object('config')

try:
    bucket_name = os.environ['WL_DATASOURCE_BUCKET']
except:
    bucket_name = 'wl_datasource'

attribute_weights = {
    'PERSON': {
        'name': {
            'attribute_weight': 0.8,
            'matching_strategies_weights': {
                'exact': 1.0,
                'exact_transposition': 1.0,
                'exact_transposition_light_denoise': 1.0,
                'exact_transposition_medium_denoise': 1.0,
                'exact_transposition_heavy_denoise': 1.0,
                'starts_with': 1.0,
                'ngram': 1.0,
                'fuzzy': 1.0,
                'edit_distance': 1.0,
                'initials': 1.0,
                'query_string': 1.0
            }
        },
        'date_of_birth': {
            'attribute_weight': 0.1
        },
        'place_of_birth': {
            'attribute_weight': 0.1,
            'filter_by': True
        }
    },
    'ORGANIZATION': {
        'name': {
            'attribute_weight': 0.8,
            'matching_strategies_weights': {
                'exact': 1.0,
                'exact_transposition': 1.0,
                'starts_with': 1.0,
                'fuzzy': 1.0,
                'edit_distance': 1.0,
                'abbreviation': 1.0,
                'fingerprint': 1.0,
                'query_string': 1.0
            }
        },
        'date_of_registration': {
            'attribute_weight': 0.1
        },
        'place_of_registration': {
            'attribute_weight': 0.1,
            'filter_by': True
        }
    }
}


def get_datestamp() -> str:
    return datetime.datetime.now().strftime('%Y%m%d')


# Redis backend

# HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


# Home page view
index_name_prefix = 'index/'
data_source_prefix = 'xml/'


def load_saved_org_index(source_bucket_name, index_name):
    destination_blob_name = index_name_prefix + index_name
    logger.info("Index name - " + index_name)

    read_path = '/bst/app/src/module/screening/' + index_name

    logger.info(check_timestamp(bucket_name, destination_blob_name, read_path))

    if check_timestamp(bucket_name, destination_blob_name, read_path):
        logger.info('Newer File Found on GCS - Updating..')
        os.remove(read_path)
        read_index(source_bucket_name, destination_blob_name, index_name)
        logger.info('Index update complete.')
    else:
        logger.info('Already have the current version. Skipping..')

    return EntityMatcherIndex.load(read_path)


def load_saved_person_index(source_bucket_name, index_name):
    destination_blob_name = index_name_prefix + index_name
    logger.info("Index name - " + index_name)

    read_path = '/bst/app/src/module/screening/' + index_name

    logger.info(check_timestamp(bucket_name, destination_blob_name, read_path))

    if check_timestamp(bucket_name, destination_blob_name, read_path):
        logger.info('Newer File Found on GCS - Updating..')
        read_index(source_bucket_name, destination_blob_name, index_name)
        logger.info('Index update complete.')
    else:
        logger.info('Already have the current version. Skipping..')

    return EntityMatcherIndex.load(read_path)


person_index_prefix, org_index_prefix = list_indices(bucket_name, index_name_prefix)

if person_index_prefix is not None:
    logger.info('Fetching Person Indices .. ')
    person_index = load_saved_person_index(bucket_name, person_index_prefix)
    em = EntityMatcher(person_index)
else:
    logger.info('Failed to fetch indices.')

if org_index_prefix is not None:
    logger.info('Fetching Org Indices .. ')
    org_index = load_saved_org_index(bucket_name, org_index_prefix)
    em_org = OrgEntityMatcher(org_index)
else:
    logger.info('Failed to fetch indices.')


@watchlist.route('/', methods=['GET', 'POST'])
def home():
    return "No implementation on root level"


@watchlist.route('/v1/watchlist-screening/person', methods=['POST'])
def screening_person():
    try:
        pe = PrepareEntity()
        proccesed_payload, conf_level, error, max_responses = pe.prepare_screening_json(request.get_json())
        logger.info(proccesed_payload)
        if not error:
            try:
                response_body = em.start_screening(request.get_json()['entities'], float(conf_level), max_responses)
                logger.info(response_body)

                response = {
                    "results": response_body

                }

                # _response = {
                #     "results": response_body
                #
                # }
                #
                # _filter = Filter(json.dumps(_response), max_responses, 'PERSON')
                #
                # response = _filter.main()

            except Exception as e:
                print(str(e))
                response = {"type": "error", "message": str(e)}
                raise

            return response

        else:
            return error

    except Exception as e:
        logger.error(e)
        raise
        # return Response(eh.output_error(500, e), status=500, mimetype='application/json')


# specify entity_type in the API call
@watchlist.route('/v1/watchlist-screening/organization', methods=['POST'])
def screening_org():
    try:
        pe = PrepareOrgEntity()
        proccesed_payload, conf_level, error, max_responses = pe.prepare_screening_json(request.get_json())
        logger.info(proccesed_payload)
        wl_id = '201912052200'

        if not error:
            try:
                response_body = em_org.start_org_screening(request.get_json()['entities'], float(conf_level),
                                                           max_responses)
                logger.info(response_body)

                response = {
                    "results": response_body

                }

                # _response = {
                #     "results": response_body
                #
                # }
                #
                # _filter = Filter(json.dumps(_response), max_responses, 'ORG')
                #
                # response = _filter.main()


            except Exception as e:
                print(str(e))
                response = {"type": "error", "message": str(e)}
                raise

            return response
        else:
            return error

    except Exception as e:
        logger.error(e)
        return Response(json.dumps({"Type": "Error", "message": "screening failed " + str(e)}, indent=4), status=500)
