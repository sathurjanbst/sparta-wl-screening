# Import flask and template operators
from flask import Flask, render_template, request,send_from_directory
from flask import Response
from datetime import timedelta

from module.router import watchlist


# Define WSGI object
app = Flask(__name__, static_folder='static', static_url_path='')
app.register_blueprint(watchlist, url_prefix='/')
app.config.from_object('config')
app.config['MYSQL_USER'] = "root"
app.config['MYSQL_PASSWORD'] = ""
app.config['JSON_SORT_KEYS'] = False
app.permanent_session_lifetime = timedelta(minutes=app.config['SESSION_TTL'])

