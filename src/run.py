from module.app import app
from werkzeug.middleware.proxy_fix import ProxyFix
import logging


print("___SPARTA WATCHLIST ENGINE__ Is Starting...\n")

app.wsgi_app = ProxyFix(app.wsgi_app)

#set Session secreat key
app.secret_key = '2Us53wgy8mQKvdA6fVBkDzcX3MVDdBkN-eB35YjbmiFn5hUYdBaMDR7wpv2TTVvKV'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=False, threaded=False, processes=1)

#sudo yum install python37-setuptools