#project data
version=1.0.24
root=$PWD
container_name=sparta-wl-screening-dev
project_name=sparta-dev3
region=us-east1
cluster_name=sparta-dev3-backend-cluster
source_bucket_name=wl_datasource
replicas_count=20
container_port=8000

##config data##
WL_DATASOURCE_BUCKET=sparta-dev3-dj-input

